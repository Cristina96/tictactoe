import React from "react";
import InputSettings from "../../components/inputSettings/input";
import SelectColor from "../../components/selectColorRedux/selectColor";
import "./styless.scss";

const Settings: React.FC = () => {
  return (
    <div className="container-settings">
      <h2>Settings</h2>
      <form>
        <InputSettings name={"X"} playerIdx={0} />
        <InputSettings name={"O"} playerIdx={1} />
        <SelectColor />
      </form>
    </div>
  );
};

export default Settings;
