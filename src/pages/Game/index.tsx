import React, { useState } from "react";
import GameComponent from "../../components/game2/game";
import TableGame from "../../components/tableGame/tableGame";
import {
  GlobalGameStateContext,
  GameState,
  GameStatus,
  CurrentPlayer,
} from "../../shared/context";
import "./styles.scss";

const Game: React.FC = () => {
  const [state, setState] = useState<GameState>({
    status: GameStatus.Play,
    currentPlayer: CurrentPlayer.X,
  });
  return (
    <GlobalGameStateContext.Provider value={{ state, setState }}>
      <div className="container-game-page">
        <GameComponent />
        <TableGame />
      </div>
    </GlobalGameStateContext.Provider>
  );
};

export default Game;
