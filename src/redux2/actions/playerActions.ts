import { PlayerI } from "../../shared/type";
//
// export type PlayerAction = {
//   readonly type: string;
//   player: PlayerI;
// };
//
// export type PlayerState = {
//   players: PlayerI[];
// };
// export type DispatchType = (args: PlayerAction) => PlayerAction;

//
export interface IPlayer {
  readonly type: string;
  player?: PlayerI;
}
//
// export interface IPlayerO {
//   readonly type: "SET_PLAYER_O";
//   playerChangeO?: PlayerT;
// }
//
// export type PlayerActionsX = IPlayerX;
// export type PlayerActionsO = IPlayerO;
export type PlayerActions = IPlayer;
export const SET_PLAYER_X = "SET_PLAYER_X";
export const SET_PLAYER_O = "SET_PLAYER_O";
