import { colorTypes } from "../../shared/constants";

export interface IColorActions {
  readonly type: "SET_COLOR";
  color?: colorTypes;
}
export type ColorActions = IColorActions;
