import { createStore } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";

import rootReducer from "../reducers/rootReducer";
import { ColorPage } from "../reducers/colorReducer";
import { ColorActions } from "../actions/colorActions";
//import PlayerStateX from "../reducers/playerReducerX";
//import PlayerStateO from "../reducers/playerReducerO";
//import { PlayerActionsX, PlayerActionsO } from "../actions/playerActions";
import { PlayerState } from "../reducers/playerReducer";
import { PlayerActions } from "../actions/playerActions";

const store = createStore<
  { status: ColorPage; players: PlayerState },
  ColorActions | PlayerActions,
  null,
  null
>(rootReducer, devToolsEnhancer({}));

export default store;
