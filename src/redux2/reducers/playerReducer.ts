import * as Actions from "../actions/playerActions";
import { PlayerI } from "../../shared/type";

export type PlayerState = {
  players: PlayerI[];
};

const initialState: PlayerState = {
  players: [
    {
      playerName: "X",
    },
    {
      playerName: "O",
    },
  ],
};

const PlayerReducer = (
  state: PlayerState = initialState,
  action: Actions.PlayerActions
): PlayerState => {
  const players = state.players;
  switch (action.type) {
    case Actions.SET_PLAYER_X:
      players[0] = action.player ? action.player : initialState.players[0];
      return {
        ...state,
        players,
      };
    case Actions.SET_PLAYER_O:
      players[1] = action.player ? action.player : initialState.players[1];
      return {
        ...state,
        players,
      };
    default:
      return state;
  }
};

export default PlayerReducer;
