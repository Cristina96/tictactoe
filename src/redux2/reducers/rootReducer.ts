import { combineReducers } from "redux";
import ColorReducer from "./colorReducer";
// import PlayerReducerX from "./playerReducerX";
// import PlayerReducerO from "./playerReducerO";
import PlayerReducer from "./playerReducer";

const rootReducer = combineReducers({
  status: ColorReducer,
  players: PlayerReducer,
});

export type AppColor = ReturnType<typeof rootReducer>;

export default rootReducer;
