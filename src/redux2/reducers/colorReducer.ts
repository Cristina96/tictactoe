import { ColorActions } from "../actions/colorActions";
import { colorTypes } from "../../shared/constants";

export type ColorPage = {
  status: colorTypes;
};

const initialState: ColorPage = {
  status: colorTypes.White,
};

const ColorReducer = (
  state: ColorPage = initialState,
  action: ColorActions
): ColorPage => {
  switch (action.type) {
    case "SET_COLOR":
      return {
        ...state,
        status: action.color as colorTypes,
      };
    default:
      return state;
  }
};

export default ColorReducer;
