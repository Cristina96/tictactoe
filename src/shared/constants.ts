export enum StatusGameTypes {
  Playing = "playing",
  FinishWin = "win",
  FinishTie = "tie",
}

export enum colorTypes {
  Snow = "snow",
  Cornsilk = "cornsilk",
  Lavender = "lavender",
  Cyan = "lightcyan",
  Yellow = "lightyellow",
  White = "white",
}
