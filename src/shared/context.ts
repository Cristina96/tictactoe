import { createContext, useContext } from "react";

export enum GameStatus {
  Play = "play",
  Win = "win",
  Tie = "tie",
}

export enum CurrentPlayer {
  X = "X",
  O = "O",
}

export type GameState = {
  status: GameStatus;
  currentPlayer: CurrentPlayer;
};

type GlobalGameState = {
  state?: GameState;
  setState?: (state: GameState) => void;
};

export const GlobalGameStateContext = createContext<GlobalGameState>({});

export const useGlobalGameStateContext = (): GlobalGameState =>
  useContext(GlobalGameStateContext);
