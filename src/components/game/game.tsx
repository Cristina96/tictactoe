import React, { useState, useCallback, useEffect } from "react";
// import { useSelector } from "react-redux";
// import { AppState } from "../../redux/reducers/rootReducer";
// import "./game.scss";
// import Board from "../board/board";
// //import Children from "../children";
//
// interface GameI {
//   player: boolean | null;
//   end: boolean;
// }
// interface titleGameI {
//   titleStateGame: string;
// }
//
// //-----FC--------------
// const Game: React.FC = () => {
//   //--VAR---
//   const [playerState, setPlayer] = useState<GameI>({
//     player: true,
//     end: false,
//   });
//   const { player } = playerState;
//   const { status } = useSelector((state: AppState) => state.status);
//   const playerName = player ? "X" : "O";
//   const [titleGame, setTitleGame] = useState<titleGameI>({
//     titleStateGame: `Player's turn: ${playerName}`,
//   });
//   const { titleStateGame } = titleGame;
//
//   //---HOOKS---
//   useEffect(() => {
//     console.log("Effect Game");
//     switch (status as string) {
//       case "win":
//         setTitleGame({ ...titleGame, titleStateGame: `Winner: ${playerName}` });
//         break;
//       case "playing":
//         setTitleGame({
//           ...titleGame,
//           titleStateGame: `Player's turn: ${playerName}`,
//         });
//         break;
//       case "tie":
//         setTitleGame({ ...titleGame, titleStateGame: `Tie` });
//         break;
//     }
//   }, [status, player]);
//
//   //-----FUNCTIONS----
//   const handleCallback = useCallback(
//     (endGame) => {
//       if (endGame) {
//         setPlayer({ ...playerState, end: endGame });
//       } else {
//         const newPlayerBool = !player;
//         setPlayer({ ...playerState, player: newPlayerBool });
//       }
//     },
//     [playerState, player]
//   );
//
//   //----RENDER----
//   return (
//     <div>
//       <h2>{titleStateGame}</h2>
//       <Board namePlayer={playerName} callbackParent={handleCallback} />
//       {/*<Children>{": )"}</Children>*/}
//     </div>
//   );
// };
//
// export default Game;
