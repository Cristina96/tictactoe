import React, { Dispatch, useCallback, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { AppColor } from "../../redux2/reducers/rootReducer";
import * as Actions from "../../redux2/actions/playerActions";

import * as HttpService from "../../services/uinames";

interface InputI {
  name: string;
  playerIdx: number;
}

interface NamesI {
  name: string;
}

const InputSettings: React.FC<InputI> = ({ name, playerIdx }: InputI) => {
  //-----VAR----
  const { players } = useSelector((state: AppColor) => state.players);
  const player = players[playerIdx].playerName;
  //const inputRef = useRef() as MutableRefObject<HTMLInputElement>;

  const playerDispatch = useDispatch<Dispatch<Actions.PlayerActions>>();

  const [namesGenerator, setNamesGenerator] = useState<NamesI[]>();
  const getNames = async (): Promise<void> => {
    const req = await HttpService.get<any>(
      `https://api.fungenerators.com/name/categories.json?start=0&limit=100`
    );
    const { parsedBody } = req;
    const { contents } = parsedBody;
    const listNames = contents[0].map((i: any) => i.name);
    //console.log(listNames);
    setNamesGenerator(listNames);
  };

  //---HOOKS----

  useEffect(() => {
    getNames() as unknown;
  }, []);

  const handleSetPlayer = useCallback(
    (value: string) => {
      playerDispatch({
        type: `SET_PLAYER_${name}`,
        player: { playerName: value },
      });
    },
    [playerDispatch] // eslint-disable-line react-hooks/exhaustive-deps
  );

  const assingName = () => {
    const num = Math.floor(Math.random() * 24);
    const newName = namesGenerator
      ? namesGenerator.map((name) => name)[num]
      : "NaN";
    handleSetPlayer(newName as string);
  };

  return (
    <div>
      <label>
        {`Player ${name}:`}
        <input
          type="text"
          //ref={inputRef}
          placeholder={player}
          value={player}
          onChange={(event) => {
            handleSetPlayer(event.target.value);
          }}
        />
      </label>
      <input
        type="button"
        value="Random Name"
        onClick={() => {
          assingName();
        }}
      />
    </div>
  );
};

export default InputSettings;
