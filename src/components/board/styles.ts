import Styled from "styled-components";

interface divBoard {
  pointerEvents: string;
}
const divStyled = Styled.div<divBoard>`

box-sizing: content-box;
width: 200px;
height: 200px;

padding: 2px;
margin: 1em 3em;
border: 2px solid black;
background-color: white;

display: grid;
grid-template-columns: repeat(3, 1fr);
grid-template-rows: repeat(3, 1fr);
pointer-events: ${({ pointerEvents }) =>
  pointerEvents === "true" ? "none" : "all"};
`;

export default divStyled;
