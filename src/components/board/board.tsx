import React, { useState, useEffect } from "react";
//import { GameActions } from "../../redux/actions/gameActions";
//import { StatusGameTypes } from "../../shared/constants";

import "./board.scss";
import Square from "../square/square";
import StyleDiv from "./styles";
//import { useDispatch } from "react-redux";
import {
  useGlobalGameStateContext,
  GameState,
  GameStatus,
  CurrentPlayer,
} from "../../shared/context";

interface BoardI {
  squares: string[];
  endGame: boolean;
}

// interface GameParentI {
//   namePlayer: string;
//   callbackParent: (i: boolean) => void;
// }

const initialState: BoardI = {
  squares: Array(9).fill(""),
  endGame: false,
};

//-----FC------
// const Board: React.FC<GameParentI> = ({
//   namePlayer,
//   callbackParent,
// }: GameParentI) => {
const Board: React.FC = () => {
  //----VAR-----
  const [squaresState, setSquares] = useState<BoardI>(initialState);

  const { squares, endGame } = squaresState;
  //const { squares } = squaresState;
  //const statusGameDispatch = useDispatch<Dispatch<GameActions>>();
  const { setState, state } = useGlobalGameStateContext();
  const { status, currentPlayer } = state as GameState;

  const setStatePlayerFun = () => {
    if (setState) {
      setState(
        currentPlayer === CurrentPlayer.X
          ? { status, currentPlayer: CurrentPlayer.O }
          : { status, currentPlayer: CurrentPlayer.X }
      );
    }
  };

  const setStateGameFun = (newState: GameStatus) => {
    if (setState) {
      setState({ status: newState, currentPlayer });
    }
  };
  //----HOOKS----
  useEffect(() => {
    if (squares.every((i) => Boolean(i))) {
      console.log("No Winner");
      // statusGameDispatch({
      //   type: "SET_STATUSGAME",
      //   gameload: StatusGameTypes.FinishTie,
      // });
      setStateGameFun(GameStatus.Tie);
      setSquares({
        ...squaresState,
        squares,
        endGame: true,
      });
      //callbackParent(true);
    }
  }, [currentPlayer]);

  //----FUNCTIONS----
  const handleOnClickCallback = (index: number) => {
    //console.warn(index, "Test");
    squares[index] = currentPlayer;
    const winSquares = checkWinner(squares);
    const newEndGame = winSquares.length === 0 ? false : true;
    console.log(newEndGame);
    if (newEndGame) {
      console.log("win");
      winSquares.forEach((element) => (squares[element] = `${currentPlayer}W`));
      // statusGameDispatch({
      //   type: "SET_STATUSGAME",
      //   gameload: StatusGameTypes.FinishWin,
      // })
      setStateGameFun(GameStatus.Win);
    } else {
      setStatePlayerFun();
    }
    //callbackParent(newEndGame);

    setSquares({
      ...squaresState,
      squares,
      endGame: newEndGame,
    });
  };

  const newBoard = () => {
    console.log("New game");
    setSquares({
      ...squaresState,
      squares: Array(9).fill(""),
      endGame: false,
    });
    // statusGameDispatch({
    //   type: "SET_STATUSGAME",
    //   gameload: StatusGameTypes.Playing,
    // });
    setStateGameFun(GameStatus.Play);
  };

  function checkWinner(squares: string[]) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return lines[i];
      }
    }
    return [];
  }

  //----RENDER----
  return (
    <div className="container-board">
      <StyleDiv pointerEvents={endGame.toString()}>
        {squares.map((element: string, index) => (
          <Square
            key={index}
            value={element}
            disabled={endGame}
            handleOnClick={() => handleOnClickCallback(index)}
          />
        ))}
      </StyleDiv>
      <button className="resetGame" onClick={newBoard}>
        NEW GAME
      </button>
    </div>
  );
};

export default Board;
