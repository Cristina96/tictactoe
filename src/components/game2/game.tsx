import React, { useMemo } from "react";
import "./game.scss";
import Board from "../board2/board";
import {
  useGlobalGameStateContext,
  GameState,
  GameStatus,
} from "../../shared/context";
import { useSelector } from "react-redux";
import { AppColor } from "../../redux2/reducers/rootReducer";

//-----FC--------------
const GameComponent: React.FC = () => {
  //--VAR---
  const { state } = useGlobalGameStateContext();
  const { players } = useSelector((state: AppColor) => state.players);

  //--HOOKS---
  const titleStateGame = useMemo(() => {
    const { status, currentPlayer } = state as GameState;
    const playerX = players[0].playerName;
    const playerO = players[1].playerName;

    const currentNamePlayer = currentPlayer === "X" ? playerX : playerO;

    switch (status as GameStatus) {
      case GameStatus.Win:
        return `Winner: ${currentNamePlayer}`;
      case GameStatus.Play:
        return currentNamePlayer === "X" || currentNamePlayer === "O"
          ? `Player's turn: ${currentNamePlayer}`
          : `${currentNamePlayer}'s turn`;
      case GameStatus.Tie:
        return `Tie`;
    }
  }, [state]); // eslint-disable-line react-hooks/exhaustive-deps

  //----RENDER----
  return (
    <div className="container-game-component">
      <h2>{titleStateGame}</h2>
      <Board />
    </div>
  );
};

export default GameComponent;
