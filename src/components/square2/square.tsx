import React from "react";
import ButtonStyled from "./styles";

interface cellSquare2 {
  value: string;
  handleOnClick?: React.MouseEventHandler<HTMLButtonElement>;
}

const Square: React.FC<cellSquare2> = ({
  value,
  handleOnClick,
}: cellSquare2) => {
  return (
    <ButtonStyled
      className="Square"
      onClick={handleOnClick}
      disabled={Boolean(value)}
      winSquare={value[1] === "W"}
    >
      {value[0]}
    </ButtonStyled>
  );
};

export default Square;
