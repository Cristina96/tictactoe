import React, { Dispatch, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { AppColor } from "../../redux2/reducers/rootReducer";
import { colorTypes } from "../../shared/constants";
import { ColorActions } from "../../redux2/actions/colorActions";

type LogColor = keyof typeof colorTypes;

const SelectColor: React.FC = () => {
  //-----VAR----
  const { status } = useSelector((state: AppColor) => state.status);
  const statusDispatch = useDispatch<Dispatch<ColorActions>>();
  //---HOOKS----
  const handleSetStatus = useCallback(
    (key: LogColor) => {
      const valueColor = colorTypes[key];
      statusDispatch({
        type: "SET_COLOR",
        color: valueColor as colorTypes,
      });
    },
    [statusDispatch]
  );

  return (
    <label>
      Color:
      <select
        defaultValue={
          Object.keys(colorTypes)[Object.values(colorTypes).indexOf(status)]
        }
        onChange={(event) => {
          handleSetStatus(event.target.value as LogColor);
        }}
      >
        {Object.keys(colorTypes).map((element: string, index: number) => (
          <option value={element} key={index}>
            {element}
          </option>
        ))}
      </select>
    </label>
  );
};

export default SelectColor;
