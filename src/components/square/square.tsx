import React from "react";
import ButtonStyled from "./styles";

interface cellSquare {
  value: string;
  disabled: boolean;
  handleOnClick?: React.MouseEventHandler<HTMLButtonElement>;
}

const Square: React.FC<cellSquare> = ({
  value,
  disabled,
  handleOnClick,
}: cellSquare) => {
  return (
    <ButtonStyled
      className="Square"
      onClick={handleOnClick}
      disabled={disabled || Boolean(value)}
      winSquare={value[1] === "W"}
    >
      {value[0]}
    </ButtonStyled>
  );
};

export default Square;
