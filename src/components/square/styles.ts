import Styled from "styled-components";

interface buttonSquare {
  winSquare: boolean;
}
const buttonStyled = Styled.button<buttonSquare>`
cursor: pointer;
border: 1px solid black;
font-size: x-large;

&:disabled,
&[disabled],
&[disabled]:hover {
  color: black;
  background-color: rgb(230, 230, 230);
}

&:hover {
  background-color: lightblue;
}

background-color:${({ winSquare }) => (winSquare ? "lightgreen" : "white;")};
&[disabled],&[disabled]:hover{
  background-color:${({ winSquare }) => (winSquare ? "lightgreen" : "white;")};
}

`;

export default buttonStyled;
