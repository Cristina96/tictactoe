import React from "react";
import { NavLink } from "react-router-dom";
import "./styles.scss";

const Header: React.FC = () => (
  <header>
    <div className="container-header">
      <ul className="links-pages">
        <li>
          <NavLink
            className="link"
            activeClassName="is-active"
            to="/"
            exact={true}
          >
            <div className="link-name">Game</div>
          </NavLink>
        </li>
        <li>
          <NavLink
            className="link"
            activeClassName="is-active"
            to="./settings"
            exact={true}
          >
            <div className="link-name">Settings</div>
          </NavLink>
        </li>
      </ul>
    </div>
  </header>
);

export default Header;
