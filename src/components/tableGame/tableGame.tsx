import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { AppColor } from "../../redux2/reducers/rootReducer";
import { useGlobalGameStateContext, GameState } from "../../shared/context";
import "./styles.scss";

interface CounterI {
  cont: number;
  winsX: number;
  winsO: number;
  ties: number;
}

const TableGame: React.FC = () => {
  //---VAR----
  const { players } = useSelector((state: AppColor) => state.players);
  const playerX = players[0].playerName;
  const playerO = players[1].playerName;
  const { state } = useGlobalGameStateContext();
  const { status, currentPlayer } = state as GameState;
  const [totalGames, setTotalGames] = useState<CounterI>({
    cont: 0,
    winsX: 0,
    winsO: 0,
    ties: 0,
  });

  //----HOOKS---
  // const incrementCont = useCallback((value:number) => {
  //   return ++value)
  // }, [setTotalGames]);

  useEffect(() => {
    const { cont, winsX, winsO, ties } = totalGames;
    if (status !== "play") {
      if (status === "tie") {
        setTotalGames({ ...totalGames, cont: cont + 1, ties: ties + 1 });
      } else {
        if (currentPlayer === "X") {
          setTotalGames({
            ...totalGames,
            cont: cont + 1,
            winsX: winsX + 1,
          });
        } else {
          setTotalGames({
            ...totalGames,
            cont: cont + 1,
            winsO: winsO + 1,
          });
        }
      }
      //incrementCont();
    }
  }, [status]); // eslint-disable-line react-hooks/exhaustive-deps
  //---RENDER---
  return (
    <div className="container-table">
      <table>
        <caption>Status Games</caption>
        <thead>
          <tr>
            <th></th>
            <th>{playerX === "X" ? `Player ${playerX}` : playerX}</th>
            <th>{playerO === "O" ? `Player ${playerO}` : playerO}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="wins td-title">Won Games</td>
            <td className="wins-x">{totalGames.winsX}</td>
            <td className="wins-o">{totalGames.winsO}</td>
          </tr>
          <tr>
            <td className="tie  td-title">Tied Games</td>
            <td className="tie-x">{totalGames.ties}</td>
            <td className="tie-o">{totalGames.ties}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td className="total td-title">Total Games</td>
            <td className="no-draw total">{totalGames.cont}</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
};

export default TableGame;
