import React, { useState, useEffect, useCallback } from "react";
import "./board.scss";
import Square from "../square2/square";
import StyleDiv from "./styles";

import {
  useGlobalGameStateContext,
  GameState,
  GameStatus,
  CurrentPlayer,
} from "../../shared/context";

interface BoardI {
  squares: string[];
}

const initialState: BoardI = {
  squares: Array(9).fill(""),
};

//-----FC------

const Board: React.FC = () => {
  //----VAR-----
  const [squaresState, setSquares] = useState<BoardI>(initialState);
  const { squares } = squaresState;
  const { setState, state } = useGlobalGameStateContext();
  const { status, currentPlayer } = state as GameState;

  //Context change
  const setStatePlayerFun = () => {
    if (setState) {
      setState(
        currentPlayer === CurrentPlayer.X
          ? { status, currentPlayer: CurrentPlayer.O }
          : { status, currentPlayer: CurrentPlayer.X }
      );
    }
  };

  const setStateGameFun = (newState: GameStatus) => {
    if (setState) {
      setState({ status: newState, currentPlayer });
    }
  };

  //----HOOKS----
  const setTie = useCallback(() => {
    if (squares.every((i: string) => Boolean(i))) {
      setStateGameFun(GameStatus.Tie);
      setSquares({
        ...squaresState,
        squares,
      });
    }
  }, [currentPlayer]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setTie();
  }, [setTie]);

  //----FUNCTIONS----
  const handleOnClickCallback = (index: number) => {
    squares[index] = currentPlayer;
    const winSquares = checkWinner(squares);
    const endGame = winSquares.length === 0 ? false : true;
    if (endGame) {
      winSquares.forEach((element) => (squares[element] = `${currentPlayer}W`));
      setStateGameFun(GameStatus.Win);
    } else {
      setStatePlayerFun();
    }
    setSquares({
      ...squaresState,
      squares,
    });
  };

  const newBoard = () => {
    console.log("New game");
    setSquares({
      ...squaresState,
      squares: Array(9).fill(""),
    });
    setStateGameFun(GameStatus.Play);
  };

  function checkWinner(squares: string[]) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return lines[i];
      }
    }
    return [];
  }

  //----RENDER----
  return (
    <div className="container-board">
      <StyleDiv pointerEvents={status.toString()}>
        {squares.map((element: string, index) => (
          <Square
            key={index}
            value={element}
            handleOnClick={() => handleOnClickCallback(index)}
          />
        ))}
      </StyleDiv>
      <button className="resetGame" onClick={newBoard}>
        NEW GAME
      </button>
    </div>
  );
};

export default Board;
