import Styled from "styled-components";

interface divBoard {
  pointerEvents: string;
}
const divStyled = Styled.div<divBoard>`

box-sizing: content-box;
width: 210px;
height: 210px;


border: 2px solid black;
background-color: white;

display: grid;
grid-template-columns: repeat(3, 1fr);
grid-template-rows: repeat(3, 1fr);
pointer-events: ${({ pointerEvents }) =>
  pointerEvents === "play" ? "all" : "none"};
`;

export default divStyled;
