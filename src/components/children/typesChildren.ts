import React from "react";

export interface ChildrenI {
  children: React.ReactNode;
}
