import React from "react";

import * as Types from "./typesChildren";
import "./styles.scss";

const Title: React.FC<Types.ChildrenI> = ({ children }: Types.ChildrenI) => (
  <div className="children">
    <h1>{children}</h1>
  </div>
);

export default Title;
