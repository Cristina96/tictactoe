interface HttpResponse<T> extends Response {
  parsedBody?: T;
}

const http = async <T>(request: RequestInfo): Promise<HttpResponse<T>> => {
  const response: HttpResponse<T> = await fetch(request);

  try {
    (response.parsedBody as unknown) = await response.json();
  } catch (ex) {
    throw new Error(ex);
  }

  if (!response.ok) {
    throw new Error(response.statusText);
  }
  return response;
};

export const get = async <T>(
  path: string,
  args: RequestInit = {
    method: "get",
    headers: {
      "Content-Type": "application/json",
    },
  }
): Promise<HttpResponse<T>> => await http<T>(new Request(path, args));
