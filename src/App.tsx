//import React from "react";
import "./App.scss";
//import Game from "./components/game/game";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import * as Pages from "./pages";
import Header from "./components/header";
import Title from "./components/children";

import { useSelector } from "react-redux";
import { AppColor } from "./redux2/reducers/rootReducer";

import StyledDivIndex from "./styledIndex";
// function App() {
//   return (
//     <div className="App">
//       <h1>Tic Tac Toe</h1>
//       <div className="container">
//         <Game />
//       </div>
//     </div>
//   );
// }
//
// export default App;

export const App: React.FC = () => {
  const { status } = useSelector((state: AppColor) => state.status);
  return (
    <BrowserRouter>
      <StyledDivIndex colorRedux={status}>
        <Title>{"TIC TAC TOE"}</Title>
        <Header />
        <Switch>
          <Route path="/" exact>
            <Pages.Game />
          </Route>
          <Route path="/settings" exact>
            <Pages.Settings />
          </Route>
        </Switch>
      </StyledDivIndex>
    </BrowserRouter>
  );
};
