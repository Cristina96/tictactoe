import { GameActions } from "../actions/gameActions";

import { StatusGameTypes } from "../../shared/constants";

export type StatusGame = {
  status: StatusGameTypes;
};

const initialState: StatusGame = {
  status: StatusGameTypes.Playing,
};

const StatusGameReducer = (
  state: StatusGame = initialState,
  action: GameActions
): StatusGame => {
  switch (action.type) {
    case "SET_STATUSGAME":
      return {
        ...state,
        status: action.gameload ? action.gameload : StatusGameTypes.Playing,
      };
    default:
      return state;
  }
};

export default StatusGameReducer;
