import { combineReducers } from "redux";
import StatusGameReducer from "./statusGameReducer";

const rootReducer = combineReducers({
  status: StatusGameReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
