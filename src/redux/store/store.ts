import { createStore } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";

import rootReducer from "../reducers/rootReducer";
import { StatusGame } from "../reducers/statusGameReducer";
import { GameActions } from "../actions/gameActions";

const store = createStore<{ status: StatusGame }, GameActions, null, null>(
  rootReducer,
  devToolsEnhancer({})
);

export default store;
