import { StatusGameTypes } from "../../shared/constants";

export interface ISetStatusGameActions {
  readonly type: "SET_STATUSGAME";
  gameload?: StatusGameTypes;
}
export type GameActions = ISetStatusGameActions;
