import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import "./reset.scss";
//import App from "./App";
import { App } from "./App";
import reportWebVitals from "./reportWebVitals";

import { Provider } from "react-redux";
import store from "./redux2/store/store";

//ReactDOM.render(<App />, document.getElementById("root"));
ReactDOM.render(
  <div>
    <Provider store={store}>
      <App />
    </Provider>
  </div>,
  document.getElementById("root")
);

reportWebVitals();
