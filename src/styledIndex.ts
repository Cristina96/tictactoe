import Styled from "styled-components";

interface divIndex {
  colorRedux: string;
}
const divStyled = Styled.div<divIndex>`

width: 100vw;
height: 100vh;
background-color: blue;

background-color: ${({ colorRedux }) => colorRedux};
`;
export default divStyled;
